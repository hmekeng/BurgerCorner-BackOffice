<?php
require 'database.php'; 
// ici on initialise les variables $nameError, $descriptionError etc.. au premier passage 

$nameError=$descriptionError=$priceError=$categoryError=$imageError=$name=$description=$price=$category=$image="";

// deuxieme pasage verifions si le POST n'est pas vide
//$_POST mon superglobal est un array
if(!empty($_POST))
{
     $name           =checkInput($_POST['name']);
     $description    =checkInput($_POST['description']);
     $price          =checkInput($_POST['price']);
     $category       =checkInput($_POST['category']);
     $image          =checkInput($_FILES['image']['name']);
     $imagePath      = '../images/'.basename($image);
     $imageExtension = pathinfo($imagePath, PATHINFO_EXTENSION);
     $isSuccess      = true;
     $isUploadSuccess =false;
    
    
    if(empty($name))
    {
        $nameError='ce champ ne peut pas être vide';
        $isSuccess      = false;    
    }
    
    if(empty($description))
    {
        $descriptionError='ce champ ne peut pas être vide';
        $isSuccess      = false;    
    }
    if(empty($price))
    {
        $priceError='ce champ ne peut pas être vide';
        $isSuccess      = false;    
    }
    if(empty($category))
    {
        $categoryError='ce champ ne peut pas être vide';
        $isSuccess      = false;    
    }
    if(empty($image))
    {
        $imageError='ce champ ne peut pas être vide';
            $isSuccess  = false;    
    }
    else{
      $isUploadSuccess =true;
        // permet de verifier l extension de l image
        if($imageExtension != "jpg" && $imageExtension != "png" && $imageExtension != "jpeg" && $imageExtension != "gif") 
        {
           $imageError ="Les fichiers autorises sont: .jpg, .jpeg, .png, .gif" ; 
            $isUploadSuccess=false;
        }
        // on verifie ici si une image portant le meme nom existe dejà
        // la fonction file_exists permet de verifier si l image existe dejà
        if(file_exists($imagePath))
        {
            $imageError = "le fichier existe déjà";
             $isUploadSuccess=false;
            
        }
        if($_FILES["image"]["size"] > 500000){
            
              $imageError ="Le fichier ne doit pas depasser les 500KB" ; 
               $isUploadSuccess=false;
            
        }
        //move_uploaded_file est une fonction
        //move_uploaded_file prend notre image temporaire et met ds le vrai chemin imagePath
        if($isUploadSuccess){
            if(!move_uploaded_file($_FILES["image"]["tmp_name"], $imagePath)){
                $imageError = "il y a eu une erreur lors de l'upload";
                $isUploadSuccess=false;
            }
        }
    }
    // tous les etapes plus haut ont été verifie on envie de rajouter l image ds notre db
    // on cree notre requete prepare 
    
    if( $isSuccess && $isUploadSuccess)
    {
        $db = Database::connect(); //connection open
        $statement = $db->prepare("INSERT INTO items (name,description,price,category,image) VALUES(?, ?, ?, ?,?)");
        $statement->execute(array($name,$description,$price,$category,$image));
        Database::disconnect(); 
        header("location: index.php");
    }
    
}


 function checkInput($data)  
        {
            $data=trim($data);
            $data =stripslashes($data);
            $data=htmlspecialchars ($data);
            return $data;
        }


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Burger</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="../css/style.css">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     
     <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     
     <link href="https://fonts.googleapis.com/css?family=Holtwood+One+SC" rel="stylesheet" type="text/css"> 
</head>
<body class="bd">
     <h1 class="text-logo"><span class="glyphicon glyphicon-cutlery"></span>BURGER CORNER<span class="glyphicon glyphicon-cutlery"></span></h1>
     
        <div class="container admin">
        <div  class="row">
               <h1><strong>Ajouter un item</strong></h1>
                <br>
<!--                 vu qu on doit upload les image on utilise (enctype="multipart/form-data")-->
                <form class="form" role="form" action="insert.php" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="name">Nom:</label><br>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nom" value="<?php echo $name; ?>" >
                    <span class="help-inline" ><?php echo $nameError; ?></span>
                  </div >
                  <div class="form-group">
                   
                     <label for="description">Description:</label><br>
                    <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="<?php echo $description; ?>" >
                    <span class="help-inline" ><?php echo $descriptionError; ?></span>
                  </div>
                  <div class="form-group">
                     <label for="price">Prix:(en €)</label><br>
                    <input type="number" step="0.01" class="form-control" id="price" name="price" placeholder="Prix" value="<?php echo $price; ?>" >
                    <span class="help-inline" ><?php echo $priceError; ?></span>
                   </div>
                  <div class="form-group">
                    <label for="category"  >Catégorie:</label><br>
<!--                   cette partie de formulaire est generée dynamiquement a partir de la db du l'injection du code php-->
                    <select name="category" id="category" class="form-control"> 
                       <?php
                        $db=Database::connect();
//                        boucle direct sur la query a chaque enregistrement il va metre le resultat dans notre variable $row
                        foreach($db->query('SELECT * FROM categories') as $row)
                        {
//                         echo 'option value="' .$row['id']. '">'  .$row['name'] . '</option>';
                           echo '<option value="'. $row['id'] .'">'. $row['name'] . '</option>';
 
                        }
                        Database::disconnect();
                        
                        ?>
                        
                    </select>
                    
                    <span class="help-inline" ><?php echo $categoryError; ?></span>
                  </div>
                  <div class="form-group">
                    <label for="image" >Selectionner une image:</label><br>
                    <input type="file" id="image" name="image">
                    <span class="help-inline" ><?php echo $imageError; ?></span>
                  </div>
                
                <br>
                <div class="form-actions">
                 <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-pencil "></span>Ajouter</button>
                  <a class="btn btn-primary" href="index.php"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a>
                </div>
            </form>
        </div>
    </div>
</body>


</html>