<!--
ceci nous permet de nous connecter a notre base de données.
on se connecte avec la PDO en creant nos variables connection
-->
<?php
class Database{
    // SET DSN
private static $dbHost="localhost";
private static $dbName="burgerdb";
private static $dbUser="root";
private static $dbUserPassword="";
 // nos variable de db etant en private static on met donc self:: devant les champs a concartener.    
   private static $connection=null;
  public static function connect(){
    try{
        // dbHost , dbName on essaie de concartener
        
    // Create a PDO instance ecrire sans espace et retour a la ligne pour eviter au max les erreurs 
    self::$connection = new PDO("mysql:host=" . self::$dbHost . ";dbname=" . self::$dbName , self::$dbUser, self::$dbUserPassword);
        
    }
    catch(PDOException $e){
        die($e->getMessage());
    }
    
       return self::$connection;
       
   }
    public static function disconnect(){
        self::$connection=null;
    }
}

Database::connect(); //on appel notre fonction pour tester notre connection
?>