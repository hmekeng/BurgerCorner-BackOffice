<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Burger</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="../css/style.css">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     
     <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     
     <link href="https://fonts.googleapis.com/css?family=Holtwood+One+SC" rel="stylesheet" type="text/css"> 
</head>
<body class="bd">
     
     
     <?php
        require 'database.php';
        //$_GET est la superglobal et les données transmis a l'url vont etre stockées dans cette //superglobal on verifie si ce n'est pas vide if not //empty 
        if(!empty($_GET['id']))  
        {
           $id = checkInput($_GET['id']); //qd on recupere notre variable $id on fait checkInput
        
        }
        // on creer notre function checkINput qui prend comme parametre $data et verifie plusieurs //choses

        $db=Database::connect(); // on se connecte a notre database 
    
      // on effectue une  requete preparée
        $statement=$db->prepare('SELECT items.id,  items.name, items.description, items.price, items.image, categories.name AS category FROM items LEFT JOIN categories ON items.category = categories.id  WHERE items.id = ?'); 
         $statement->execute(array($id));
        $item=$statement->fetch(); // iteem notre variable
        Database::disconnect();
    

        function checkInput($data)  
        {
            $data=trim($data);
            $data =stripslashes($data);
            $data=htmlspecialchars ($data);
            return $data;
        }

     ?>
     
    <h1 class="text-logo"><span class="glyphicon glyphicon-cutlery"></span>BURGER CORNER<span class="glyphicon glyphicon-cutlery"></span></h1>
    
     <div class="container admin">
        <div  class="row">
       <div class="col-sm-6">
                <h1><strong>Voir un item</strong></h1>
                <br>
                <form>
                  <div class="form-group">
                    <label>Nom:</label><?php echo '  '.$item['name'];?>
                  </div>
                  <div class="form-group">
                    <label>Description:</label><?php echo '  '.$item['description'];?>
                  </div>
                  <div class="form-group">
                    <label>Prix:</label><?php echo '  '.number_format((float)$item['price'], 2, '.', ''). ' €';?>
                  </div>
                  <div class="form-group">
                    <label>Catégorie:</label><?php echo '  '.$item['category'];?>
                  </div>
                  <div class="form-group">
                    <label>Image:</label><?php echo '  '.$item['image'];?>
                  </div>
                </form>
                <br>
                <div class="form-actions">
                  <a class="btn btn-primary" href="index.php"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a>
                </div>
        </div> 
                
          <div class="col-sm-6 site">
                <div class="thumbnail">
                    <img src="<?php echo '../images/'.$item['image'];?>" alt="...">
                    <div class="price"><?php echo number_format((float)$item['price'], 2, '.', ''). ' €';?></div>
                    <div class="caption">
                        <h4><?php echo $item['name'];?></h4>
                        <p><?php echo $item['description'];?></p>
                        <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                    </div>
                       
                 </div>
                    
                </div>
           
            </div>
           
           
         </div> 
       
     
     
</body>


</html>