<?php
  require 'database.php'; 
// au premier passage on veut recuperer notre id par la methode get au clic sur l oongelet modifier
//si je t'ai passé le id par la methode GET alors met cette valleur ds mon variable $id
    if(!empty($_GET['id']))  
        {
       $id = checkInput($_GET['id']); //qd on recupere notre variable $id on fait checkInput

        }
// pour poster mon formulaire on verifie le $_POST
   if(!empty($_POST))
   {
       $id = checkInput($_POST['id']);
       $db = Database::connect();
       $statement = $db->prepare("DELETE FROM items WHERE id = ?");
       $statement ->execute(array($id));
       Database::disconnect(); 
       header("location: index.php");
       
   }

 function checkInput($data)  
        {
            $data=trim($data);
            $data =stripslashes($data);
            $data=htmlspecialchars ($data);
            return $data;
        }


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Burger</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="../css/style.css">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     
     <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     
     <link href="https://fonts.googleapis.com/css?family=Holtwood+One+SC" rel="stylesheet" type="text/css"> 
</head>
<body class="bd">
     <h1 class="text-logo"><span class="glyphicon glyphicon-cutlery"></span>BURGER CORNER<span class="glyphicon glyphicon-cutlery"></span></h1>
     
        <div class="container admin">
        <div  class="row">
               <h1><strong>Supprimer un item</strong></h1>
                <br>
<!--                 vu qu on doit upload les image on utilise (enctype="multipart/form-data")-->
                <form class="form" role="form" action="delete.php" method="post" >
                 <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                 <p class="alert alert-warning">Etes vous sur de vouloir supprimer?</p>
               
                <div class="form-actions">
                 <button type="submit" class="btn btn-warning">Oui</button>
                  <a class="btn btn-default" href="index.php">Non</a>
                </div>
            </form>
        </div>
    </div>
</body>


</html>