<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Burger</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="../css/style.css">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     
     <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     
     <link href="https://fonts.googleapis.com/css?family=Holtwood+One+SC" rel="stylesheet" type="text/css"> 
</head>
<body class="bd">
     <h1 class="text-logo"><span class="glyphicon glyphicon-cutlery"></span>BURGER CORNER<span class="glyphicon glyphicon-cutlery"></span></h1>
     
     <div class="container admin">
        <div  class="row">
           <h1><strong>Liste des items</strong><a href="insert.php" class="btn btn-success btn-lg"><span class="glyphicon glyphicon-plus"></span>Ajouter</a></h1>
           <table class="table table-striped table-bordered">
               <thead>
                  <tr>
                      <th>Nom</th>
                      <th>Description</th>
                      <th>Prix</th>
                      <th>Catégories</th>
                      <th>Action</th>
                  </tr>
                   
               </thead>
               <tbody>

                    <?php
                        require 'database.php';
                        $db=Database::connect(); // ouverture de connection a la database
                        // on effectue une  query PDO
                        // l'atlias As  categories.name=category
                        $statement=$db->query('SELECT items.id, items.name, items.description, items.price, categories.name AS category FROM items LEFT JOIN categories ON items.category = categories.id '  );

                    //while($donnees ou item =$statement-> fetch(PDO::FETCH_ASSOC) )
                 if($statement != false) {
                   // affichage des données dans un tableau
                     while($item = $statement->fetch()) 
                        {
                            echo '<tr>';
                            echo '<td>'. $item['name'] . '</td>';
                            echo '<td>'. $item['description'] . '</td>';
                         // number_format et 2 pour maintenir deux chiffre apres la virgule 
                            echo '<td>'. number_format((float)$item['price'], 2, '.', '') . '</td>';  
                            echo '<td>'. $item['category'] . '</td>';
                            echo '<td width=300>';
                            echo '<a class="btn btn-default" href="view.php?id='.$item['id'].'"><span class="glyphicon glyphicon-eye-open"></span> Voir</a>';
                            echo ' ';
                            echo '<a class="btn btn-primary" href="update.php?id='.$item['id'].'"><span class="glyphicon glyphicon-pencil"></span> Modifier</a>';
                            echo ' ';
                            echo '<a class="btn btn-danger" href="delete.php?id='.$item['id'].'"><span class="glyphicon glyphicon-remove"></span> Supprimer</a>';
                            echo '</td>';
                            echo '</tr>';
                        }
                 }
                        Database::disconnect(); // fermeture de la connection 
                             
                    ?>                  
                  
                                    
               </tbody>
           </table>
            
        </div>
         
     </div>
</body>
</html>