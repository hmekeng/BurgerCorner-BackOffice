<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Burger</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="css/style.css">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
     
     <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     
     <link href="https://fonts.googleapis.com/css?family=Holtwood+One+SC" rel="stylesheet" type="text/css"> 
</head>
<!--http://www.burgerking.co.uk/-->
<body class="bd">
   <div class="container site">
      <h1 class="text-logo"><span class="glyphicon glyphicon-cutlery"></span>BURGER CORNER<span class="glyphicon glyphicon-cutlery"></span></h1>
<!--      //ce fichier permet de rendre entierement notre site dynamique-->
      <?php 
       require 'admin/database.php';
       // on cause par ecrire les elts qui ne change pas avec la base de donnee
        echo'<nav>
                <ul class="nav nav-pills">';
             // les li sont ecrits or on a envie de recuperer depuis notre database 
             $db = Database::connect();
             $statement = $db ->query('SELECT * FROM categories');
             $categories = $statement->fetchAll();
             foreach($categories as $category)
             {
                 if($category['id']=='1'){
                     echo '<li role="presentation" class="active"><a href="#' .$category['id'] .  '" data-toggle="tab">'.$category['name']. '</a></li>';
                 }
                 else {
                     echo '<li role="presentation"><a href="#' .$category['id'] .  '" data-toggle="tab">'.$category['name']. '</a></li>'; 
                 }
             }
        echo '</ul>';       
      echo '</nav>';
       echo ' <div class="tab-content">';
        // pour chacun des ongelet on a besoin du numero de son id
        foreach($categories as $category)
             {
                 if($category['id']=='1'){
                     echo '<div class="tab-pane active" id="'.$category['id'] .  '">';
                 }else
                 {
                echo '<div class="tab-pane" id="'.$category['id'] .  '">';
                 }
            
            echo'<div class="row">';
            $statement= $db->prepare('SELECT * FROM items WHERE items.category = ?');
            $statement->execute(array($category['id']));
            //plus haut on été dans chaque category 
            // maintenant on fait une bocle pour recuperer chaque item dans la table items de notre db
            //pour avoir chacune des lignes j utilise la fonction fetch()
            while($item =$statement->fetch())
            {
           echo '<div class="col-sm-6 col-md-4">
                   <div class="thumbnail">
                      <img src="images/'.$item['image']. '" alt="...">
                      <div class="price">' . number_format((float)$item['price'], 2, '.', ''). ' € </div>
                      <div class="caption">
                         <h4>'.$item['name'].'</h4>
                         <p>'.$item['description'].'</p>
                          <a href="#" class="btn btn-order" role="button"> <span class="glyphicon glyphicon-shopping-cart">Commander</span></a>
                      </div>
                       
                   </div>
             </div>';  
            }
        echo '</div>';
    echo '</div>';
        }
      Database::disconnect();
       echo'</div>'; // div tab-content
       
       ?>
       
        </div>;
   
         
       
   
    
</body>
</html>